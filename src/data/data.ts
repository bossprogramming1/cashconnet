export const product_per_page_options = [
  { id: 12, name: 12 },
  { id: 24, name: 24 },
  { id: 36, name: 36 },
];

export var required: any = "required";

export const ticketfileExtension =
  ".jpg,  .jpeg, .webp, .png, .doc, .pdf, .txt, .docs , .docx, .xls, .xlsx";
export const ticketImgExtensionArr = [
  ".jpg",
  ".jpeg",
  ".png",
  ".PNG",
  ".JPG",
  ".JPEG",
  ".webp",
];
export const allowedExtensions = [
  ".jpg",
  ".jpeg",
  ".png",
  ".PNG",
  ".JPG",
  ".JPEG",
  ".webp",
  ".doc",
  ".pdf",
  ".txt",
  ".docs",
  ".docx",
  ".xls",
  ".xlsx",
];

export const genders = [
  { id: "male", name: "Male" },
  { id: "female", name: "Female" },
  { id: "others", name: "Others" },
];

export const sortBy = [
  { id: "recent", name: "Recent" },
  { id: "asc", name: "Low to high" },
  { id: "desc", name: "Height to low" },
];
export const rating = [
  { id: "all", name: "All" },
  { id: 1, name: "1 Star" },
  { id: 2, name: "2 Star" },
  { id: 3, name: "3 Star" },
  { id: 4, name: "4 Star" },
  { id: 5, name: "5 Star" },
];
