import { BASE_URL } from "@data/constants";
import useWindowSize from "@hook/useWindowSize";
import React, { useState } from "react";
import { getDateDifference } from "../../utils/utils";
import Avatar from "../avatar/Avatar";
import Box from "../Box";
import FlexBox from "../FlexBox";
import Rating from "../rating/Rating";
import { H6, SemiSpan } from "../Typography";
import Image from "@component/Image";

export interface ProductCommentProps {
  name;
  imgUrl: string;
  rating: number;
  created_at: string;
  review: string;
  user: {
    first_name: string;
    last_name: string;
    image: string;
  };
  review_rating_images: [
    {
      id: number;
      image: string;
    }
  ];
}

const ProductComment: React.FC<ProductCommentProps> = ({
  rating,
  created_at,
  review,
  review_rating_images,
  user,
}) => {
  const [selectedAvatar, setSelectedAvatar] = useState(null);
  const width = useWindowSize();
  const isMobile = width < 769;

  const handleClick = (avatar) => {
    if (selectedAvatar === avatar) {
      setSelectedAvatar(null);
    } else {
      setSelectedAvatar(avatar);
    }
  };

  return (
    <Box mt="15px" borderBottom={"1px solid #DAE1E7"}>
      <Box>
        <FlexBox alignItems="center">
          <Rating value={rating} color="warn" readonly />
          <H6 mx="10px">{rating}</H6>
          <SemiSpan>{getDateDifference(created_at)}</SemiSpan>
        </FlexBox>
        <FlexBox alignItems="center">
          <SemiSpan mb="4px" mr="4px">{`by ${user?.first_name}`}</SemiSpan>
          <FlexBox alignItems="center">
            <Image src="https://img.icons8.com/fluency/20/null/verified-account.png" />
            <SemiSpan style={{ color: "#4caf50" }} ml="4px">
              Verified Purchase
            </SemiSpan>
          </FlexBox>
        </FlexBox>
      </Box>

      <H6 color="gray.700">{review}</H6>
      <br />
      {review_rating_images && (
        <FlexBox
          mb="15px"
          style={{ justifyContent: "flex-start", gap: "10px" }}
          maxWidth={260}
        >
          {review_rating_images.slice(0, 6).map((url) => (
            <Box
              size={isMobile ? 56 : 100}
              minWidth={isMobile ? 56 : 100}
              bg="white"
              display="flex"
              justifyContent="center"
              alignItems="center"
              cursor="pointer"
              border="1px solid"
              key={url.id}
              borderColor={selectedAvatar === url ? "primary.main" : "gray.400"}
              onClick={() => handleClick(url)}
            >
              <Avatar
                src={`${BASE_URL}${url?.image}`}
                radius={1}
                size={isMobile ? 50 : 94}
              />
            </Box>
          ))}
        </FlexBox>
      )}

      {selectedAvatar && (
        <Box mb="15px" display="flex" width="fit-content" position="relative">
          <Avatar
            float="left"
            radius={1}
            src={`${BASE_URL}${selectedAvatar?.image}`}
            size={isMobile ? 200 : 400}
          />
        </Box>
      )}
    </Box>
  );
};

export default ProductComment;
