import Image from "@component/Image";
import React from "react";

const LoadingComponent: React.FC = () => {
  return (
    <div
      style={{
        position: "fixed",
        height: "100%",
        width: "100%",
        top: "0px",
        left: "0px",
        display: "flex",
        justifyContent: "center",
        background: "#ffffff",
        alignItems: "center",
        zIndex: 100,
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-around",
        }}
      >
        <div>
          <Image
            style={{
              height: "100px",
              width: "300px",
              display: "block",
              marginLeft: "auto",
              marginRight: "auto",
            }}
            src="/assets/images/logos/main_logo_loading.png"
            alt="Main Logo"
          />
        </div>
        <div>
          <Image
            style={{
              height: "100px",
              width: "100px",
              marginTop: "100px", // Fixed typo "100pz" to "100px"
              display: "block",
              marginLeft: "auto",
              marginRight: "auto",
            }}
            src="/assets/images/gif/loading.gif"
            alt="Loading Gif"
          />
        </div>
      </div>
    </div>
  );
};

export default LoadingComponent;
