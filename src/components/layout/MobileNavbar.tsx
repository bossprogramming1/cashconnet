import Accordion from "@component/accordion/Accordion";
import AccordionHeader from "@component/accordion/AccordionHeader";
import Divider from "@component/Divider";
import NavLink from "@component/nav-link/NavLink";
// import { useRouter } from "next/router";
import React, { Fragment } from "react";
import Typography, { Paragraph } from "../Typography";
import useUserInf from "@customHook/useUserInf";
// import Image from "@component/Image";

const MobileNavbar = () => {
  // const [loading, setLoading] = useState(false);

  // const router = useRouter();

  // const handleLoadingComplete = () => {
  //   setLoading(false);
  // };
  const { isLogin } = useUserInf();

  // useEffect(() => {
  //   router.events.on("routeChangeComplete", handleLoadingComplete);
  // }, [router.events]);

  return (
    <>
      {/* {loading && (
        <div
          style={{
            position: "fixed",
            height: "100%",
            width: "100%",
            top: "0px",
            left: "0px",
            display: "flex",
            justifyContent: "center",
            backgroundColor: " rgb(0 0 0 / 50%)",
            alignItems: "center",
            zIndex: 100,
          }}
        >
          <Image
            style={{
              height: "50px",
              width: "50px",
              marginTop: "100pz",
            }}
            src="/assets/images/gif/loading.gif"
          />
        </div>
      )} */}

      {linkLists.map((item) => (
        <Fragment key={item.id}>
          <Divider />
          {item.list ? (
            <Accordion>
              <AccordionHeader px="0px" py="10px">
                <Typography fontWeight="600" fontSize="15px">
                  {item.title}
                </Typography>
              </AccordionHeader>

              {item.list?.map((item) => (
                <NavLink
                  className="nav-link"
                  // href={item.href}
                  href={
                    !isLogin && item.href === "/sell/youritems"
                      ? "/login"
                      : item.href
                  }
                  onClick={(event) => {
                    if (!isLogin && item.href === "/sell/youritems") {
                      localStorage.setItem("backAfterLogin", "/sell/youritems");
                    }
                    event.stopPropagation(); // Prevent event from propagating to parent
                  }}
                  key={item.id}
                >
                  <Paragraph
                    className="cursor-pointer"
                    fontSize="14px"
                    fontWeight="600"
                    pl="20px"
                    py="6px"
                  >
                    {item.title}
                  </Paragraph>
                </NavLink>
              ))}
            </Accordion>
          ) : (
            <NavLink className="nav-link" href={item.href} key={item.id}>
              <Paragraph
                className="cursor-pointer"
                fontSize="14px"
                fontWeight="600"
                py="6px"
                // onClick={() => {
                //   if (item?.title === "Home") {
                //     setLoading(true);
                //   }
                // }}
              >
                {item.title}
              </Paragraph>
            </NavLink>
          )}
        </Fragment>
      ))}
    </>
  );
};

const linkLists = [
  {
    id: 1,
    title: "Home",
    href: "/",
  },
  {
    id: 2,
    title: "About Us",
    href: "/",
  },
  {
    id: 3,
    title: "Shop Now",
    list: [
      // {
      //   href: "/",
      //   title: "Shop By Store",
      // },
      {
        id: 4,
        href: "/product/search/shop_now?condition=new",
        title: "New",
      },
      {
        id: 5,
        href: "/product/search/shop_now?condition=old",
        title: "Pre Owned",
      },
    ],
  },
  // {
  //   title: "Shop Now",
  //   list: [
  //     {
  //       href: "/",
  //       title: "Shop By Store",
  //     },
  //     {
  //       href: "/",
  //       title: "New",
  //     },
  //     {
  //       href: "/",
  //       title: "Pre Owned",
  //     },
  //   ],
  // },
  {
    id: 6,
    title: "Sell",
    list: [
      {
        id: 7,
        href: "/sell/youritems",
        title: "Sell Online",
      },
      {
        id: 8,
        href: "/home",
        title: "Sell In Branch",
      },
    ],
  },
  {
    id: 9,
    title: "Get A Loan",
    href: "/policy",
  },
  {
    id: 10,
    title: "Customer Care",
    href: "/help",
  },
];

export default MobileNavbar;
