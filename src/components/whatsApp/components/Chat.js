import React, { useState, useContext } from "react";
import WidgetContext from "./Context";
import Anim from "./Anim";

export default function Chat(props) {
  const { isOpen } = useContext(WidgetContext);
  const [chatAnim, setChatAnim] = useState(true);
  const date = new Date().toTimeString().slice(0, 5);
  const bodyBgColor = props.bodyBgColor || "rgb(227, 220, 213)";
  const chatPersonName = props.chatPersonName || "Support";
  const chatMessage = props.chatMessage || (
    <>
      Hi there 👋 <br />
      <br /> How can I help you?
    </>
  );

  setTimeout(() => {
    isOpen && setChatAnim(false);
  }, 3000);

  return (
    <div
      key={props}
      className="whatsapp_widget_chat_wrapper"
      style={{
        backgroundColor: bodyBgColor,
      }}
    >
      {chatAnim ? (
        <div className="whatsapp_widget_bubble_anim">
          <div className="whatsapp_widget_bubble_anim_inner">
            <Anim />
          </div>
        </div>
      ) : (
        <div className="whatsapp_widget_chat_wrapper_inner">
          <div className="whatsapp_widget_chat_wrapper_arrow" />
          <div
            style={{
              fontSize: 12,
              color: "rgba(0, 0, 0, 0.4)",
            }}
          >
            {chatPersonName}
          </div>
          <div
            style={{
              fontSize: 13,
              color: "#111",
            }}
          >
            {chatMessage}
          </div>
          <div
            style={{
              fontSize: 12,
              color: "rgba(0, 0, 0, 0.4)",
              textAlign: "end",
              marginTop: 5,
            }}
          >
            {date}
          </div>
        </div>
      )}
    </div>
  );
}
