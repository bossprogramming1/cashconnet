import React, { useContext } from "react";
import WidgetContext from "./Context";

export default function AppIcon(props) {
  const { handleOpen } = useContext(WidgetContext);

  const iconSize = props.iconSize ? parseInt(props.iconSize) : 52,
    iconBgColor = props.iconBgColor ? props.iconBgColor : "rgb(255, 255, 255)";

  return (
    <div
      key={props}
      onClick={handleOpen}
      className="whatsapp_widget_icon"
      style={{
        backgroundColor: iconBgColor,
        width: iconSize,
        height: iconSize,
      }}
    >
      <svg
        height="24"
        viewBox="0 0 24 24"
        width="24"
        xmlns="http://www.w3.org/2000/svg"
        fill="white"
      >
        <path d="M20 2H4c-1.1 0-2 .9-2 2v18l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2z"></path>
        <path d="M0 0h24v24H0z" fill="none"></path>
      </svg>
    </div>
  );
}
