import React, { useContext, useEffect, useState } from "react";
import WidgetContext from "./Context";
import Chat from "./Chat";
import Footer from "./Footer";
import Header from "./Header";

export default function Pop(props) {
  const { isOpen } = useContext(WidgetContext);
  const [status, setStatus] = useState("close");

  useEffect(() => {
    // Set the status to 'open' or 'close' after a delay
    setTimeout(() => {
      setStatus(isOpen ? "open" : "close");
    }, 300);
  }, [isOpen]);

  // If the widget is closed, render nothing
  if (!isOpen) return <React.Fragment />;

  return (
    <div key={props} className={`whatsapp_widget_pop ${status}`}>
      <Header {...props} />
      <Chat {...props} />
      <Footer {...props} />
    </div>
  );
}
