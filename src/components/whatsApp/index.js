import React from "react";
import { WhatsAppWidgetContextProvider } from "./components/Context";
import Widget from "./components/Widget";

export default function WhatsAppWidget(props) {
  return (
    <WhatsAppWidgetContextProvider>
      <Widget key={props} {...props} />
    </WhatsAppWidgetContextProvider>
  );
}
