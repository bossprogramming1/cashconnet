import { Chip } from "@component/Chip";
import { useAppContext } from "@context/app/AppContext";
import useUserInf from "@customHook/useUserInf";
import { Customer_Order_Pending_Details } from "@data/constants";
import axios from "axios";
// import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import useWindowSize from "../../hooks/useWindowSize";
import Icon from "../icon/Icon";
import NavLink from "../nav-link/NavLink";
import StyledMobileNavigationBar from "./MobileNavigationBar.style";
import Sidenav from "@component/sidenav/Sidenav";
import CustomerDashboardNavigation from "@component/layout/CustomerDashboardNavigation";
// import Image from "@component/Image";

const MobileNavigationBar: React.FC = () => {
  const width = useWindowSize();
  const [_reRender, setReRender] = useState(0);
  const [productQuantity, setProductQuantity] = useState(0);
  const { state } = useAppContext();
  const cartCanged = state.cart.chartQuantity;
  const [open, setOpen] = useState(false);
  const toggleSidenav = () => setOpen(!open);

  // try {
  //   var user_type: string = localStorage.getItem("userType");
  // } catch (error) {
  //   var user_type: string = "customer";
  // }

  // const [loading, setLoading] = useState(false);

  // const router = useRouter();

  const { isLogin } = useUserInf();

  // const handleLoadingComplete = () => {
  //   setLoading(false);
  // };

  // useEffect(() => {
  //   router.events.on("routeChangeComplete", handleLoadingComplete);
  // }, [router.events]);

  useEffect(() => {
    const orderId = localStorage.getItem("OrderId");

    if (orderId) {
      axios
        .get(`${Customer_Order_Pending_Details}${orderId}`, {
          headers: {
            "Content-type": "application/json",
            Authorization: localStorage.getItem("jwt_access_token"),
          },
        })
        .then((res) => {
          let sum = 0;
          res?.data?.order?.order_items.map((e) => (sum = sum + e.quantity));

          setProductQuantity(sum);
        })
        .catch(() => {
          setProductQuantity(0);
        });
    }
  }, [cartCanged]);

  try {
    var userID: string = localStorage.getItem("UserId");
  } catch (err) {
    var userID = "";
  }
  useEffect(() => {
    setReRender(Math.random());
  }, [userID]);

  const list = [
    {
      id: 1,
      title: "Home",
      icon: "home",
      href: "/",
    },
    {
      id: 2,
      title: "Categories",
      icon: "category",
      href: "/mobile-category-nav",
    },
    {
      id: 3,
      title: "Cart",
      icon: "bag",
      href: isLogin ? "/cart" : "/login",
    },
    {
      id: 4,
      title: isLogin ? "Account" : "Login",
      icon: isLogin ? "user-3" : "user-2",
      href: isLogin ? "" : "/login",
    },
  ];

  return (
    width <= 900 && (
      <>
        {/* {loading && (
          <div
            style={{
              position: "fixed",
              height: "100%",
              width: "100%",
              top: "0px",
              left: "0px",
              display: "flex",
              justifyContent: "center",
              backgroundColor: " rgb(0 0 0 / 50%)",
              alignItems: "center",
              zIndex: 100,
            }}
          >
            <Image
              style={{
                height: "50px",
                width: "50px",
                marginTop: "100pz",
              }}
              src="/assets/images/gif/loading.gif"
            />
          </div>
        )} */}

        <StyledMobileNavigationBar>
          {list.map((item) => (
            <React.Fragment key={item.id}>
              <NavLink
                style={{
                  display: item.title === "Account" ? "none" : "flex",
                }}
                className="link"
                href={item?.href}

                // onClick={() => {
                //   if (item?.title === "Home") {
                //     setLoading(true);
                //   }
                // }}
              >
                <Icon className="icon" variant="small">
                  {item.icon === "user-3" ? "" : item.icon}
                </Icon>
                {item.title === "Account" ? "" : item.title}

                {item.title === "Cart" && !!productQuantity && (
                  <Chip
                    bg="primary.main"
                    position="absolute"
                    color="primary.text"
                    fontWeight="600"
                    px="0.25rem"
                    top="4px"
                    left="calc(50% + 8px)"
                  >
                    {productQuantity || 0}
                  </Chip>
                )}
              </NavLink>
              {item.title === "Account" && (
                <>
                  <Sidenav
                    key={item.id}
                    open={open}
                    toggleSidenav={toggleSidenav}
                    position="left"
                    handle={
                      <div className="link">
                        <Icon className="icon">{item.icon}</Icon>
                        {item.title}
                      </div>
                    }
                  >
                    <CustomerDashboardNavigation
                      toggleSidenav={toggleSidenav}
                    />
                  </Sidenav>
                </>
              )}
            </React.Fragment>
          ))}
        </StyledMobileNavigationBar>
      </>
    )
  );
};

export default MobileNavigationBar;
