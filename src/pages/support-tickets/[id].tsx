import Avatar from "@component/avatar/Avatar";
import Box from "@component/Box";
import Button from "@component/buttons/Button";
import Divider from "@component/Divider";
import FlexBox from "@component/FlexBox";
import Grid from "@component/grid/Grid";
import DashboardLayout from "@component/layout/CustomerDashboardLayout";
import DashboardPageHeader from "@component/layout/DashboardPageHeader";
import TextArea from "@component/textarea/TextArea";
import Typography, { H5, SemiSpan } from "@component/Typography";
import useUserInf from "@customHook/useUserInf";
import {
  BASE_URL,
  Ticket_Details_All,
  Ticket_Details_Create,
} from "@data/constants";
import {
  allowedExtensions,
  ticketImgExtensionArr,
  ticketfileExtension,
} from "@data/data";
import axios from "axios";
import { format } from "date-fns";
import { useFormik } from "formik";
import jsonToFormData from "helper/jsonToFormData";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState, useRef, useCallback } from "react";
import * as yup from "yup";
import Icon from "@component/icon/Icon";
import Modal from "@component/modal/Modal";
import Card from "@component/Card";
import useWindowSize from "@hook/useWindowSize";
import LazyImage from "@component/LazyImage";

const PaymentMethodEditor = () => {
  const [messagelist, setMessagelist] = useState([]);
  const [reloadMessage, setReloadMessage] = useState(0);
  const [previewImage, setPreviewImage] = useState([]);
  const [images, setImages] = useState([]);
  const [_reRender, setReRender] = useState(0);
  const [showImage, setShowImage] = useState("");
  const [errorText, setErrorText] = useState("");

  const inputFileRef = useRef(null);
  const width = useWindowSize();
  const isMobile = width < 769;

  const [open, setOpen] = useState(false);
  const router = useRouter();
  const { id } = router.query;

  const { user_id, authTOKEN } = useUserInf();

  const toggleDialog = useCallback(() => {
    setOpen((open) => !open);
  }, []);

  const getExtenstion = (url: string) => {
    const fileUrlStr = url;
    const extNameIndx = fileUrlStr.lastIndexOf(".");
    const extName = extNameIndx >= 0 ? fileUrlStr.slice(extNameIndx) : "";

    return extName;
  };

  const showFile = (fileUrl: string) => {
    const extStr = getExtenstion(fileUrl);

    const isImage = ticketImgExtensionArr.find((url) => url === extStr);

    if (isImage) {
      setOpen(true);
      setShowImage(fileUrl);
    } else {
      window.open(fileUrl);
    }
  };

  const cancelAImage = (imgId) => {
    let newPreImgs = [...previewImage];
    newPreImgs.splice(imgId, 1);
    setPreviewImage(newPreImgs);

    let newImages = [...images];
    newImages.splice(imgId, 1);
    setImages(newImages);

    setReRender(Math.random());
  };

  useEffect(() => {
    if (id) {
      // setReloadMessage(Math.random());
      axios
        .get(`${Ticket_Details_All}${id}`, {
          headers: {
            "Content-type": "application/json",
            Authorization: localStorage.getItem("jwt_access_token"),
          },
        })
        .then((res) => {
          setFieldValue("message", "");
          setMessagelist(res?.data?.ticket_details);
        })
        .catch(() => {});
    }
  }, [id, reloadMessage]);

  const handleFormSubmit = async () => {
    const invalidFiles = images.filter((file) => {
      const extension = file.name.split(".").pop();
      return !allowedExtensions.includes(`.${extension}`);
    });

    if (invalidFiles.length > 0) {
      setErrorText(
        `Invalid file(s): ${invalidFiles.map((file) => file.name).join(", ")}`
      );
      return;
    }

    const data = {
      ticket: id,
      message: values.message,
      customer: user_id,
      file: images,
    };

    const [ticketDetailsFormData] = jsonToFormData(data);

    axios
      .post(`${Ticket_Details_Create}`, ticketDetailsFormData, authTOKEN)
      .then((res) => {
        res?.data?.id && setReloadMessage(Math.random());
        setPreviewImage([]);
        setImages([]);
        if (inputFileRef.current) {
          inputFileRef.current.value = "";
        }
      })
      .catch((err) => {
        setErrorText(err.response.data.detail);
      });
  };

  useEffect(() => {
    setTimeout(() => {
      setErrorText("");
    }, 5000);
  }, [errorText]);

  // const handleFormSubmit = async () => {
  //   const data = {
  //     ticket: id,
  //     message: values.message,
  //     customer: user_id,
  //     file: images,
  //   };

  //   const [ticketDetailsFormData] = jsonToFormData(data);

  //   axios
  //     .post(`${Ticket_Details_Create}`, ticketDetailsFormData, authTOKEN)
  //     .then((res) => {
  //       res?.data?.id && setReloadMessage(Math.random());
  //       setPreviewImage([]);
  //       setImages([]);
  //       if (inputFileRef.current) {
  //         inputFileRef.current.value = "";
  //       }
  //     })
  //     .catch((err) => {
  //
  //       setErrorText(err.response.data.detail);
  //       setTimeout(() => {
  //         setErrorText("");
  //       }, 5000);
  //     });
  // };

  const {
    values,
    errors,
    touched,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldValue,
  } = useFormik({
    initialValues: initialValues,
    validationSchema: checkoutSchema,
    onSubmit: handleFormSubmit,
  });

  return (
    <div>
      <DashboardPageHeader
        iconName="support"
        title="Support Ticket"
        button={
          <Link href="/support-tickets" as="/support-tickets">
            <Button color="primary" bg="primary.light" px="2rem">
              Back to Support Ticket
            </Button>
          </Link>
        }
      />
      <Modal open={open} onClose={toggleDialog}>
        <Card
          p="1rem"
          position="relative"
          style={{
            minWidth: isMobile ? "100%" : "400px",
            maxWidth: "100%",
            overflow: "hidden",
          }}
        >
          <LazyImage
            src={`${showImage}`}
            width="100%"
            height="100%"
            layout="responsive"
            loader={() => `${showImage}`}
            alt={"not found"}
          />

          <div
            style={{
              position: "absolute",
              right: "10px",
              top: "10px",
            }}
          >
            <Icon
              className="close"
              color="primary"
              variant="small"
              style={{
                cursor: "pointer",
              }}
              onClick={() => setOpen(false)}
            >
              close
            </Icon>
          </div>
        </Card>
      </Modal>

      {messagelist?.map((item) => {
        return (
          <FlexBox
            mb="15px"
            key={item.id}
            style={{
              justifyContent: item?.customer ? "flex-end" : "flex-start",
            }}
          >
            <Box>
              <H5 fontWeight="600" mt="0px" mb="0px">
                {item?.customer?.name || item?.admin?.name}
              </H5>

              {item?.customer && (
                <FlexBox alignItems="center" justifyContent="flex-end">
                  <div>
                    <Box
                      borderRadius="10px"
                      bg="gray.200"
                      textAlign={"justify"}
                      p="1rem"
                      mr="1rem"
                      style={
                        {
                          // whiteSpace: "pre",
                          // textAlign: item?.customer ? "right" : "left",
                          // direction: item?.customer ? "rtl" : "ltr",
                        }
                      }
                    >
                      {item?.message}
                    </Box>
                  </div>
                  <Avatar
                    src={`${
                      item?.customer_image
                        ? `${
                            item?.customer_image !== "/media/"
                              ? `${BASE_URL}${item?.customer_image}`
                              : "/no_image.png"
                          }`
                        : "/no_image.png"
                    }`}
                    ml={item?.customer?.name && "1rem"}
                  />
                </FlexBox>
              )}
              {!item?.customer && (
                <FlexBox alignItems="center" justifyContent="flex-end">
                  <Avatar
                    src={`${
                      item?.admin_image
                        ? `${
                            item?.admin_image != "/media/"
                              ? `${BASE_URL}${item?.admin_image}`
                              : "/no_image.png"
                          }`
                        : "/no_image.png"
                    }`}
                    mr={item?.admin?.name && "1rem"}
                  />
                  <div>
                    <Box
                      borderRadius="10px"
                      bg="gray.200"
                      p="1rem"
                      ml="1rem"
                      textAlign={"justify"}
                      style={
                        {
                          // whiteSpace: "pre",
                          // textAlign: item?.customer ? "right" : "left",
                          // direction: item?.customer ? "rtl" : "ltr",
                        }
                      }
                    >
                      {item?.message}
                    </Box>
                  </div>
                </FlexBox>
              )}

              <Box display="flex" justifyContent="flex-end" mt="10px">
                {item?.images &&
                  item?.images.map((e) => (
                    <Box mr="10px" key={e.image}>
                      <Avatar
                        radius={10}
                        onClick={() => {
                          showFile(`${BASE_URL}${e.image}`);
                        }}
                        src={`${BASE_URL}${e.image}`}
                        size={50}
                      />
                    </Box>
                  ))}
              </Box>
              <Box mt="10px" display="flex" justifyContent="flex-end">
                {item?.files &&
                  item.files.map((file) => {
                    const extension = file.file.split(".").pop().toLowerCase();
                    let icon = "";
                    switch (extension) {
                      case "pdf":
                        icon = "pdf-file";
                        break;
                      case "doc":
                      case "docx":
                        icon = "word-file";
                        break;
                      case "txt":
                        icon = "txt-file";
                        break;
                      case "xls":
                      case "xlsx":
                        icon = "xls-file";
                        break;
                      default:
                        icon = "word-file";
                        break;
                    }
                    return (
                      <Box mr="10px" key={file.file}>
                        <a
                          href={`${BASE_URL}${file.file}`}
                          target="_blank"
                          download
                          rel="noopener noreferrer"
                        >
                          <Icon
                            variant="large"
                            style={{
                              cursor: "pointer",
                            }}
                          >
                            {icon}
                          </Icon>
                        </a>
                      </Box>
                    );
                  })}
              </Box>
              <SemiSpan style={{ direction: "ltr" }}>
                <pre
                  style={{
                    margin: "0px",
                    wordSpacing: "-5px",
                    textAlign: item?.customer ? "right" : "left",
                  }}
                >
                  {item?.created_at &&
                    format(new Date(item?.created_at), "hh:mm:a | dd MMM yyyy")}
                </pre>
              </SemiSpan>
            </Box>
          </FlexBox>
        );
      })}

      <Divider mb="2rem" bg="gray.300" />
      <form onSubmit={handleSubmit}>
        <TextArea
          name="message"
          placeholder="Write your message here..."
          rows={8}
          borderRadius={8}
          fullwidth
          onBlur={handleBlur}
          onChange={handleChange}
          value={values.message || ""}
          errorText={touched.message && errors.message}
        />

        <Grid item xs={12}>
          <Box
            display="flex"
            flexDirection="column"
            justifyContent="center"
            alignItems="flex-start"
            minHeight="100px"
            paddingLeft="12px"
            border="1px dashed"
            borderColor="gray.400"
            borderRadius="10px"
            transition="all 250ms ease-in-out"
            mb="1.5rem"
            style={{ outline: "none" }}
          >
            <Typography
              color="gray.700"
              fontSize="20px"
              fontWeight="bold"
              mb="5px"
            >
              Attachment
            </Typography>

            <input
              multiple
              onChange={async (e) => {
                const reader: any = new FileReader();
                reader.onload = () => {
                  if (reader.readyState === 2) {
                    let newImg = [...previewImage];

                    newImg.push(reader.result);
                    setPreviewImage(newImg);
                  }
                };
                reader.readAsDataURL(e.target.files[0]);

                const file = e.target.files[0];
                let newImgFile = [...images];

                newImgFile.push(file);

                setImages(newImgFile);
                // onChange(file);
              }}
              ref={inputFileRef}
              id="profile-image"
              accept={ticketfileExtension}
              type="file"
            />

            {errorText && (
              <Typography mt="5px" mb="5px" color="error.main">
                {errorText}
              </Typography>
            )}
            <Typography mt="5px" mb="5px">
              {`(Allowed File Extensions: ${ticketfileExtension.replace(
                /\./g,
                ""
              )}`}
            </Typography>

            <Box
              width="80%"
              mt="25px"
              display="flex"
              style={{ gap: "20px" }}
              justifyContent="center"
              flexWrap="wrap"
            >
              {previewImage?.map((src, id) => {
                // Get the file extension
                const extension = images[id]?.name
                  ?.split(".")
                  ?.pop()
                  ?.toLowerCase();

                // Define the icons for different file types
                let icon = null;
                switch (extension) {
                  case "pdf":
                    icon = "pdf-file";
                    break;
                  case "doc":
                  case "docx":
                    icon = "word-file";
                    break;
                  case "txt":
                    icon = "txt-file";
                    break;
                  case "xls":
                  case "xlsx":
                    icon = "xls-file";
                    break;
                  // Add more cases for other file types
                  default:
                    icon = "word-file";
                    break;
                }

                return (
                  <>
                    <Box display="flex" width="fit-content" position="relative">
                      <div
                        id="cancelIcon"
                        style={{
                          position: "absolute",
                          top: "-10px",
                          right: "-10px",
                          zIndex: 1,
                          color: "red",
                        }}
                      >
                        <Icon
                          onClick={() => {
                            cancelAImage(id);
                          }}
                        >
                          cancel
                        </Icon>
                      </div>

                      {extension === "pdf" ||
                      extension === "doc" ||
                      extension === "docx" ||
                      extension === "txt" ||
                      extension === "xls" ||
                      extension === "xlsx" ? (
                        <Icon
                          variant="extralarge"
                          style={{
                            cursor: "pointer",
                          }}
                        >
                          {icon}
                        </Icon>
                      ) : (
                        <Avatar float="left" radius={10} src={src} size={50} />
                      )}
                    </Box>
                  </>
                );
              })}
            </Box>
          </Box>
        </Grid>

        <Button
          variant="contained"
          color="primary"
          ml="auto"
          type="submit"
          // onClick={() => handleFormSubmit()}
        >
          Post message
        </Button>
      </form>
    </div>
  );
};

const initialValues = {
  message: "",
};

const checkoutSchema = yup.object().shape({
  message: yup.string().required("required"),
});

// const messageListss = [
//   {
//     imgUrl: "/assets/images/faces/face-7.jpg",
//     name: "Esther Howard",
//     date: "2020-12-14T08:39:58.219Z",
//     text:
//       "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ipsum velit amet, aliquam massa tellus. Condimentum sit at pharetra, congue. Sit mattis amet nec pharetra odio. Interdum lorem vestibulum et amet et duis placerat. Ac mattis massa duis mi tellus sed. Mus eget in fames urna, ornare nunc, tincidunt tincidunt interdum. Amet aliquet pharetra rhoncus scelerisque pulvinar dictumst at sit. Neque tempor tellus ac nullam. Etiam massa tempor eu risus fusce aliquam.",
//   },
//   {
//     imgUrl: "/assets/images/faces/10.jpg",
//     name: "Ralph Edwards",
//     date: "2021-01-05T05:39:58.219Z",
//     text:
//       "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ipsum velit amet, aliquam massa tellus. Condimentum sit at pharetra, congue. Sit mattis amet nec pharetra odio. Interdum lorem vestibulum et amet et duis placerat.",
//   },
//   {
//     imgUrl: "/assets/images/faces/face-7.jpg",
//     name: "Esther Howard",
//     date: "2021-01-14T08:39:58.219Z",
//     text:
//       "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nunc, lectus mi ornare. Bibendum proin euismod nibh tellus, phasellus.",
//   },
// ];

PaymentMethodEditor.layout = DashboardLayout;

export default PaymentMethodEditor;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
