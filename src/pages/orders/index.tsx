import CustomerDashboardLayout from "@component/layout/CustomerDashboardLayout";
import CustomerOrderList from "@component/orders/CustomerOrderList";
import React from "react";

const Orders = () => {
  return <CustomerOrderList />;
};

Orders.layout = CustomerDashboardLayout;

export default Orders;

export async function getServerSideProps() {
  // This is an empty async function to satisfy the requirement of getServerSideProps
  return { props: {} };
}
