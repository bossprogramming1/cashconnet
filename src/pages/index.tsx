import Section13 from "@component/home/Section13";
import {
  Category_Top_All,
  Category_With_Product_Brand,
  Category_Wth_Name_Img,
  Get_Pending_Order_After_Login,
  Product_Arrival,
  Product_Discount,
  Product_Flash_Deals,
  Product_For_You,
  Product_Top_Rated,
  Slider_All,
} from "@data/constants";
import getFormattedProductData from "@helper/getFormattedProductData";
import axios from "axios";
import { useEffect, useState } from "react";

import Section1 from "../components/home/Section1";
import Section10 from "../components/home/Section10";
import Section11 from "../components/home/Section11";
import Section12 from "../components/home/Section12";
import Section2 from "../components/home/Section2";
import Section3 from "../components/home/Section3";
import Section4 from "../components/home/Section4";
import Section5 from "../components/home/Section5";
import Section7 from "../components/home/Section7";
import AppLayout from "../components/layout/AppLayout";
// import Image from "@component/Image";

import useUserInf from "@customHook/useUserInf";
import WhatsAppWidget from "@component/whatsApp";

const IndexPage = () => {
  // const [loading, setLoading] = useState(false);
  const [apiCallsMade, setApiCallsMade] = useState(false);
  const [sectionShow, setSectionShow] = useState(false);

  const { isLogin } = useUserInf();
  const [sliderList, setSliderList] = useState([]);
  const [flashDealsList, setFlashDealsList] = useState([]);
  const [topCategoryList, setTopCategoryList] = useState([]);
  const [topRatedList, setTopRatedList] = useState([]);
  const [newArrivalList, setNewArrivalList] = useState([]);
  const [bigDiscountList, setBigDiscountList] = useState([]);
  const [categoriesList, setCategoriesList] = useState([]);
  const [moreForYouList, setMoreForYouList] = useState([]);
  const [categoryWithProductBrandList, setCategoryWithProductBrandList] =
    useState([]);

  useEffect(() => {
    const fetchData = async () => {
      // Mark that the loading screen has been shown
      try {
        // Fetch slider data
        const sliderRes = await axios.get(`${Slider_All}`);
        const sliderList = await sliderRes.data.homepage_sliders;
        sliderList.sort((a, b) => a.serial_number - b.serial_number);
        setSliderList(sliderList);
        // const hasLoadingScreenBeenShown = sessionStorage.getItem(
        //   "hasLoadingScreenBeenShown"
        // );

        // if (!hasLoadingScreenBeenShown) {
        //   setLoading(true);
        //   const fetchLoadingData = async () => {
        //     try {
        //       // Simulate a delay of 1000ms (1 second)
        //       setTimeout(() => {
        //         setLoading(false);
        //         // Mark that the loading screen has been shown
        //         sessionStorage.setItem("hasLoadingScreenBeenShown", "true");
        //       }, 3000);
        //     } catch (_error) {
        //       // Handle errors here
        //       // You might want to set loading to false in case of an error as well
        //       setLoading(false);
        //     }
        //   };

        //   fetchLoadingData();
        // } else {
        //   // If the loading screen has been shown before, set loading to false
        //   setLoading(false);
        // }
      } catch (sliderError) {
        console.error("Error fetching slider data:", sliderError);
        // Handle error for slider data
      }

      try {
        // Fetch flash deals data
        const flashDealsRes = await axios.get(
          `${Product_Flash_Deals}?page=${1}&size=${6}`
        );
        const flashDealsLists = await flashDealsRes.data.products;
        const flashDealsList = await getFormattedProductData(flashDealsLists);
        setFlashDealsList(flashDealsList);
      } catch (flashDealsError) {
        console.error("Error fetching flash deals data:", flashDealsError);
        // Handle error for flash deals data
      }

      try {
        // Fetch top category data
        const topCategoryRes = await axios.get(
          `${Category_Top_All}?page=${1}&size=${6}`
        );
        const topCategoryList = await topCategoryRes.data.categories;
        setTopCategoryList(topCategoryList);
      } catch (topCategoryError) {
        console.error("Error fetching top category data:", topCategoryError);
        // Handle error for top category data
      }
    };

    fetchData();
  }, []);

  const handleScrollToCategoryWithProductBrandList = async () => {
    try {
      setSectionShow(true);
      // Fetch new arrival data
      const newArrivalRes = await axios.get(
        `${Product_Arrival}?page=${1}&size=${6}`
      );
      const newArrivalLists = await newArrivalRes.data.products;
      const newArrivalList = await getFormattedProductData(
        newArrivalLists,
        "Arrivals"
      );
      setNewArrivalList(newArrivalList);
    } catch (newArrivalError) {
      console.error("Error fetching new arrival data:", newArrivalError);
      // Handle error for new arrival data
    }
    try {
      // Fetch category with product and brand data
      const categoryWithProductBrandRes = await axios.get(
        `${Category_With_Product_Brand}?page=${1}&size=${10}`
      );
      const categoryWithProductBrandList = await categoryWithProductBrandRes
        .data.categories_with_products_and_brands;
      setCategoryWithProductBrandList(categoryWithProductBrandList);
    } catch (categoryWithProductBrandError) {
      console.error(
        "Error fetching category with product and brand data:",
        categoryWithProductBrandError
      );
      // Handle error for category with product and brand data
    }
    try {
      // Fetch top rated data
      const topRatedRes = await axios.get(
        `${Product_Top_Rated}?page=${1}&size=${4}`
      );
      const topRatedLists = await topRatedRes.data.products;
      const topRatedList = await getFormattedProductData(
        topRatedLists,
        "TopRated"
      );
      setTopRatedList(topRatedList);
    } catch (topRatedError) {
      console.error("Error fetching top rated data:", topRatedError);
      // Handle error for top rated data
    }

    // try {
    //   // Fetch featured brand data
    //   const featuredBrandRes = await axios.get(
    //     `${Brand_Featured}?page=${1}&size=${2}`
    //   );
    //   const featuredBrandLists = await featuredBrandRes.data.brands;
    //   const featuredBrandList = await getFormattedProductData(
    //     featuredBrandLists,
    //     "FeaturedBrands"
    //   );
    //   setFeaturedBrandList(featuredBrandList);
    // } catch (featuredBrandError) {
    //   console.error(
    //     "Error fetching featured brand data:",
    //     featuredBrandError
    //   );
    //   // Handle error for featured brand data
    // }

    try {
      // Fetch big discount data
      const bigDiscountRes = await axios.get(
        `${Product_Discount}?page=${1}&size=${6}`
      );
      const bigDiscountLists = await bigDiscountRes.data.products;
      const bigDiscountList = await getFormattedProductData(
        bigDiscountLists,
        "bigdiscount"
      );
      setBigDiscountList(bigDiscountList);
    } catch (bigDiscountError) {
      console.error("Error fetching big discount data:", bigDiscountError);
      // Handle error for big discount data
    }

    try {
      // Fetch more for you data
      const moreForYouRes = await axios.get(
        `${Product_For_You}?page=${1}&size=${24}`
      );
      const moreForYouLists = await moreForYouRes.data.products;
      const moreForYouList = await getFormattedProductData(moreForYouLists);
      setMoreForYouList(moreForYouList);
    } catch (moreForYouError) {
      console.error("Error fetching more for you data:", moreForYouError);
      // Handle error for more for you data
    }

    try {
      // Fetch categories data
      const categoriesRes = await axios.get(
        `${Category_Wth_Name_Img}?page=${1}&size=${12}`
      );
      const categoriesList = await categoriesRes.data.categories;
      setCategoriesList(categoriesList);
    } catch (categoriesError) {
      console.error("Error fetching categories data:", categoriesError);
      // Handle error for categories data
    }
  };

  useEffect(() => {
    const handleScroll = () => {
      if (!apiCallsMade && window.scrollY > 0) {
        // You can adjust the offset (100 in this case) as per your needs

        // Call your function when scrolling down
        handleScrollToCategoryWithProductBrandList();

        // Update the state to indicate that API calls have been made
        setApiCallsMade(true);
      }
    };

    // Attach the scroll event listener to the window
    window.addEventListener("scroll", handleScroll);

    // Clean up the event listener when the component is unmounted
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [apiCallsMade]);

  useEffect(() => {
    if (isLogin) {
      axios
        .get(`${Get_Pending_Order_After_Login}`, {
          headers: {
            "Content-type": "application/json",
            Authorization: localStorage.getItem("jwt_access_token"),
          },
        })

        .then((res) => {
          if (res.data.id) {
            localStorage.setItem("OrderId", res.data.id);
          } else {
            localStorage.removeItem("OrderId");
          }
        })
        .catch(() => {
          // setLoading(false),
          localStorage.removeItem("OrderId");
        });
    }
  }, [isLogin]);

  return (
    <main>
      {/* {loading && (
        <div
          style={{
            // backgroundImage:
            //   "url('/assets/images/logos/main_logo.png')!important",
            // backgroundPosition: "center !important",
            // backgroundRepeat: "no-repeat !important",
            // backgroundSize: "300px 100px !important",
            position: "fixed",
            height: "100%",
            width: "100%",
            top: "0px",
            left: "0px",
            display: "flex",
            justifyContent: "center",
            background: "#ffffff",
            alignItems: "center",
            zIndex: 100,
          }}
        >
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              justifyContent: "space-around",
            }}
          >
            <div>
              <Image
                style={{
                  height: "100px",
                  width: "300px",
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                src="/assets/images/logos/main_logo.png"
              />
            </div>
            <div>
              <Image
                style={{
                  height: "100px",
                  width: "100px",
                  marginTop: "100pz",
                  display: "block",
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
                src="/assets/images/gif/loading.gif"
              />
            </div>
          </div>
        </div>
      )} */}
      {topCategoryList && <Section1 sliderList={sliderList} />}
      <Section3 topCategoryList={topCategoryList} />
      <Section2 flashDealsList={flashDealsList} />
      <Section4 topRatedList={topRatedList} />
      <Section5 newArrivalList={newArrivalList} />
      <Section13 bigDiscountList={bigDiscountList} />
      <Section7 categoryWithProductBrandList={categoryWithProductBrandList} />
      <Section11 moreForYouList={moreForYouList} />
      <Section10 categoriesList={categoriesList} />

      {sectionShow && <Section12 />}
      {/* <Chat /> */}
      <WhatsAppWidget
        phoneNo="8801852041302"
        position="right"
        widgetWidth="300px"
        widgetWidthMobile="260px"
        // autoOpen={true}
        // autoOpenTimer={5000}
        messageBox={true}
        messageBoxTxt="Hi Team, is there any related service available ?"
        messageBoxColor="white"
        iconSize="60"
        iconColor="white"
        iconBgColor="#e84262"
        // headerIcon={images[currentImageIndex]}
        // headerIconColor="pink"
        headerTxtColor="black"
        headerBgColor="#f0f2f5"
        headerTitle="Cash Connect"
        headerCaption="Online"
        headerCaptionIcon="/assets/images/gif/online.gif"
        bodyBgColor="#efeae2"
        chatPersonName="Support"
        chatMessage={
          <>
            Hi there 👋 <br />
            <br /> Let us know if we can help you with anything at all.
          </>
        }
        footerBgColor="#f0f2f5"
        placeholder="Type a message.."
        btnBgColor="white"
        btnTxt="Start Chat"
        btnTxtColor="black"
      />
    </main>
  );
};

IndexPage.layout = AppLayout;

export default IndexPage;
